package authfully

import (
	"net/http"

	"github.com/gorilla/sessions"
)

// AuthSession is an abstraction of session for authentication / authorization.
type AuthSession interface {
	Save(r *http.Request, w http.ResponseWriter) error
}

// AuthSessionStore load session from a context and request.
// It also Save the session with context and response writer.
type AuthSessionStore interface {
	Get(r *http.Request) (AuthSession, error)
}

// gorillaSessionStore implements an AuthSessionStore
// for a github.com/gorilla/sessions.Store.
type gorillaSessionStore struct {
	raw  sessions.Store
	name string
}

// Get implements AuthSessionStore.Get.
func (store gorillaSessionStore) Get(r *http.Request) (s AuthSession, err error) {
	raw, err := store.raw.Get(r, store.name)
	if err != nil {
		return
	}
	return gorillaSession{raw: raw}, nil
}

// gorillaSession implements an AuthSession
// for a github.com/gorilla/sessions.Session.
type gorillaSession struct {
	raw *sessions.Session
}

// Save implements AuthSession.Save.
func (sess gorillaSession) Save(r *http.Request, w http.ResponseWriter) error {
	return sess.raw.Save(r, w)
}

// NewStoreFrom creates a store implementation from a github.com/gorilla/sessions.Store
// implementation.
func NewStoreFrom(src sessions.Store, name string) AuthSessionStore {
	return &gorillaSessionStore{
		raw:  src,
		name: name,
	}
}
