package authfully

import (
	"fmt"
	"net/http"
)

//
var validAuthorizationRequestParams = []string{
	"response_type",
	"client_id",
	"redirect_uri",
	"scope",
	"state",
	"code_challenge",
	"code_challenge_method",
}

// AuthorizationRequest contains values of an OAuth2 authorization request.
type AuthorizationRequest struct {

	// ResponseType of the request.
	//
	// REQUIRED. Value must be either "code", "token"
	// "password", or "client_credentials". Dependes on the
	// grant type wanted.
	ResponseType string

	// ClientID of the request.
	//
	// REQUIRED. The client identifier as described in RFC6749, Section 2.2.
	ClientID string

	// RedirectURI of the request.
	//
	// OPTIONAL. As described in RFC6749, Section 3.1.2.
	RedirectURI string

	// Scope of the request.
	//
	// OPTIONAL. The scope of the access request as described by RFC6749
	// Section 3.3.
	Scope string

	// State of the request.
	//
	// RECOMMENDED.  An opaque value used by the client to maintain
	// state between the request and callback.  The authorization
	// server includes this value when redirecting the user-agent back
	// to the client.  The parameter SHOULD be used for preventing
	// cross-site request forgery as described in RFC6749, Section 10.12.
	State string

	// CodeChallenge of the request.
	//
	// Specified in RFC7636 (extension of RFC6749).
	// REQUIRED. Code challenge.
	CodeChallenge string

	// CodeChallengeMethod of the request.
	//
	// Specified in RFC7636 (extension of RFC6749).
	// OPTIONAL, defaults to "plain" if not present in the request.  Code
	// verifier transformation method is "S256" or "plain".
	CodeChallengeMethod string
}

// DecodeAuthorizationRequest parses an http request into
// AuthorizationRequest or report error, if any.
func DecodeAuthorizationRequest(r *http.Request) (ar *AuthorizationRequest, err error) {
	if r == nil {
		err = fmt.Errorf("internal error: request is nil")
		return
	}
	if r.URL == nil {
		err = fmt.Errorf("internal error: request.URL is nil")
		return
	}

	// Parse the query into request struct.
	// Note: Do this early to ensure returning some AuthorizationRequest
	//       to provide "state" for error response.
	q := r.URL.Query()
	ar = &AuthorizationRequest{
		ResponseType:        q.Get("response_type"),
		ClientID:            q.Get("client_id"),
		RedirectURI:         q.Get("redirect_uri"),
		Scope:               q.Get("scope"),
		State:               q.Get("state"),
		CodeChallenge:       q.Get("code_challenge"),
		CodeChallengeMethod: q.Get("code_challenge_method"),
	}

	// Check the query.
	// 1. Should not have any parameter specified more than once.
	// 2. Should include any unsupported parameter.
	for k, v := range q {
		ok := false
		if len(v) > 1 {
			err = fmt.Errorf("parameter \"%s\" is specified more than once", k)
			return
		}
		for _, p := range validAuthorizationRequestParams {
			if ok = k == p; ok {
				break
			}
		}
		if !ok {
			err = fmt.Errorf("parameter \"%s\" is invalid", k)
			return
		}
	}

	// Validate the request.
	if ar.ResponseType == "" {
		err = fmt.Errorf("response_type is missing from the request")
		return
	}
	if ar.ClientID == "" {
		err = fmt.Errorf("client_id is missing from the request")
		return
	}
	if ar.CodeChallenge == "" {
		err = fmt.Errorf("code_challenge is missing from the request")
		return
	}
	if ar.CodeChallengeMethod != "" {
		if ar.CodeChallengeMethod != "S256" && ar.CodeChallengeMethod != "plain" {
			err = fmt.Errorf("code_challenge_method \"%s\" is not supported", ar.CodeChallengeMethod)
			return
		}
	}
	return
}

// AccessTokenRequest represents access token request.
// Specified in subsections under RFC6749 section 4.
//
// See: https://datatracker.ietf.org/doc/html/rfc6749#section-4
type AccessTokenRequest struct {
	// GrantType of the request.
	//
	// REQUIRED. Value must be either "authorization_code",
	// "password", or "client_credentials". Dependes on the
	// relevant authorization request.
	GrantType   string
	Code        string
	RedirectURI string
	ClientID    string

	// RFC7636
	CodeVerifier string
}
