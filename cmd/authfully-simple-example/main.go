package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	h := http.NewServeMux()

	h.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, `
		<!doctype html>
		<html>
		<body>
			<form>

			</form>
		</body>
		</html>
		`)
	}))

	log.Fatal(http.ListenAndServe(":8080", h))
}
