# Authfully

Yet another Go library for OAuth2 server implementation.

## License

Licensed under the GNU Lesser General Public License (LGPL)
version 3. A copy of the license can be obtained in the
[LICENSE.md](LICENSE.md) in this repository.