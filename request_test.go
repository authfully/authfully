package authfully_test

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/authfully/authfully"
)

func TestDecodeAuthorizationRequest_nilRequest(t *testing.T) {
	_, err := authfully.DecodeAuthorizationRequest(nil)
	if err == nil {
		t.Errorf("expected error but got nil")
	}
}

func TestDecodeAuthorizationRequest_nilRequestURL(t *testing.T) {
	r, _ := http.NewRequest("GET", "/authorize", nil)
	r.URL = nil
	_, err := authfully.DecodeAuthorizationRequest(nil)
	if err == nil {
		t.Errorf("expected error but got nil")
	}
}

func TestDecodeAuthorizationRequest_emptyQuery(t *testing.T) {
	r, _ := http.NewRequest("GET", "/authorize", nil)
	_, err := authfully.DecodeAuthorizationRequest(r)
	if err == nil {
		t.Errorf("expected error but got nil")
	}

	// Note: actual error doesn't matter in this test.
}

func TestDecodeAuthorizationRequest_parameterSpecifiedTwice(t *testing.T) {
	authUrl := url.URL{
		Path: "/authorize",
		RawQuery: url.Values{
			"response_type":  []string{"code", "token"},
			"client_id":      []string{"dummy_client"},
			"redirect_uri":   []string{"https://foobar.com/some/dummy/page"},
			"scope":          []string{"some-dummy-scope"},
			"state":          []string{"some-dummy-state"},
			"code_challenge": []string{"plain"},
		}.Encode(),
	}
	r, _ := http.NewRequest("GET", authUrl.String(), nil)
	_, err := authfully.DecodeAuthorizationRequest(r)
	if err == nil {
		t.Errorf("expected error but got nil")
	}
	if want, have := `parameter "response_type" is specified more than once`, err.Error(); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func TestDecodeAuthorizationRequest_invalidParameter(t *testing.T) {
	authUrl := url.URL{
		Path: "/authorize",
		RawQuery: url.Values{
			"response_type":  []string{"code"},
			"client_id":      []string{"dummy_client"},
			"redirect_uri":   []string{"https://foobar.com/some/dummy/page"},
			"scope":          []string{"some-dummy-scope"},
			"state":          []string{"some-dummy-state"},
			"code_challenge": []string{"plain"},
			"foobar":         []string{"hello"},
		}.Encode(),
	}
	r, _ := http.NewRequest("GET", authUrl.String(), nil)
	_, err := authfully.DecodeAuthorizationRequest(r)
	if err == nil {
		t.Errorf("expected error but got nil")
	}
	if want, have := `parameter "foobar" is invalid`, err.Error(); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func TestDecodeAuthorizationRequest_noResponseType(t *testing.T) {
	authUrl := url.URL{
		Path: "/authorize",
		RawQuery: url.Values{
			"client_id":      []string{"dummy_client"},
			"redirect_uri":   []string{"https://foobar.com/some/dummy/page"},
			"scope":          []string{"some-dummy-scope"},
			"state":          []string{"some-dummy-state"},
			"code_challenge": []string{"plain"},
		}.Encode(),
	}
	r, _ := http.NewRequest("GET", authUrl.String(), nil)
	_, err := authfully.DecodeAuthorizationRequest(r)
	if err == nil {
		t.Errorf("expected error but got nil")
	}
	if want, have := "response_type is missing from the request", err.Error(); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func TestDecodeAuthorizationRequest_noClientID(t *testing.T) {
	authUrl := url.URL{
		Path: "/authorize",
		RawQuery: url.Values{
			"response_type":  []string{"code"},
			"redirect_uri":   []string{"https://foobar.com/some/dummy/page"},
			"scope":          []string{"some-dummy-scope"},
			"state":          []string{"some-dummy-state"},
			"code_challenge": []string{"plain"},
		}.Encode(),
	}
	r, _ := http.NewRequest("GET", authUrl.String(), nil)
	_, err := authfully.DecodeAuthorizationRequest(r)
	if err == nil {
		t.Errorf("expected error but got nil")
	}
	if want, have := "client_id is missing from the request", err.Error(); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}

func TestDecodeAuthorizationRequest_noCodeChallenge(t *testing.T) {
	authUrl := url.URL{
		Path: "/authorize",
		RawQuery: url.Values{
			"response_type": []string{"code"},
			"client_id":     []string{"dummy_client"},
			"redirect_uri":  []string{"https://foobar.com/some/dummy/page"},
			"scope":         []string{"some-dummy-scope"},
			"state":         []string{"some-dummy-state"},
		}.Encode(),
	}
	r, _ := http.NewRequest("GET", authUrl.String(), nil)
	_, err := authfully.DecodeAuthorizationRequest(r)
	if err == nil {
		t.Errorf("expected error but got nil")
	}
	if want, have := "code_challenge is missing from the request", err.Error(); want != have {
		t.Errorf("expected %#v, got %#v", want, have)
	}
}
